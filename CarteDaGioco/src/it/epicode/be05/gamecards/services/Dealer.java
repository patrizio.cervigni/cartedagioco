package it.epicode.be05.gamecards.services;

import java.util.Collections;
import it.epicode.be05.gamecards.model.Card;
import it.epicode.be05.gamecards.model.Deck;

public class Dealer {
	private Deck deck;

	public Dealer(Deck deck) {
		this.deck = deck;
		shuffle();
	}

	public Card deal() {
		return deck.deal();
	}

	public void shuffle() {
		Collections.shuffle(deck.getCards());
	}
}
