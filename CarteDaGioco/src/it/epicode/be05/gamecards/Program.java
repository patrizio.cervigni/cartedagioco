package it.epicode.be05.gamecards;

import it.epicode.be05.gamecards.model.Card;
import it.epicode.be05.gamecards.model.FrenchCard;
import it.epicode.be05.gamecards.model.FrenchDeck;
import it.epicode.be05.gamecards.services.Dealer;

public class Program {
	public static void main(String[] args) {
		FrenchCard quattrodicuori = new FrenchCard(4, FrenchCard.Seeds.Hearts);
		// quattrodicuori.setSeed(FrenchCard.Seeds.Hearts);
		// quattrodicuori.setValue(4);
		System.out.println(quattrodicuori);

		System.out.println("Il mazzo di carte...");
		var d = new FrenchDeck();
		int counter = 1;
		while (d.hasMoreCards()) {
			System.out.format("%d\t", counter++);
			System.out.println(d.deal());
		}
		System.out.println("Il mazzo di carte distribuito dal dealer...");
		Dealer dealer = new Dealer(new FrenchDeck());
		Card c;
		counter = 1;
		while ((c = dealer.deal()) != null) {
			System.out.format("%d\t", counter++);
			System.out.println(c);
		}
	}
}
