package it.epicode.be05.gamecards.model;

// una carta francese
// E' una carta particolare (IS-A)
public class FrenchCard extends Card {

	public enum Seeds { // enum all'interno di una classe
		Hearts, // ordinal = 0 - value = "Hearts"
		Diamonds, // ordinal = 1 ...
		Clubs, Spades
	};

	public FrenchCard(int value, Seeds seed) {
		setSeed(seed);
		setValue(value);
	}
	
	// TODO: I setters sono effettivamente necessari???
	public void setSeed(Seeds seed) {
		super.setSeed(seed.ordinal());
	}

	@Override
	public void setSeed(int seed) {
		if (seed < 0 || seed > 3)
			throw new IllegalArgumentException("seed");
		super.setSeed(seed);
	}

	@Override
	public void setValue(int value) {
		if (value < 1 || value > 13)
			throw new IllegalArgumentException("value");
		super.setValue(value);
	}

	@Override
	public String toString() {
		//String[] seeds = { "Hearts", "Diamonds", "Clubs", "Spades" };
		String[] values = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

//		return String.format("%s of %s", values[getValue() - 1], seeds[getSeed()]);
		return String.format("%s of %s", values[getValue() - 1], Seeds.values()[getSeed()]);
	}
}
