package it.epicode.be05.gamecards.model;

//Una cosa che avr� un valore e un seme!
//E' una cosa perch� � ASTRATTA
//Bench� astratta essa � comunque caratterizzata
//da 2 attributi: [UN seme] e [UN valore]
//
//Carta 1 <>--------- 1 seme
//Carta 1 <>--------- 1 valore
//
//	Cosa si intende per seme?
//		E' un'informazione elementare che sar� codificata 
//		attraverso un simbolo... potrebbe anche andar bene 
//      un SEMPLICE INTERO... sicuramente � UNO SCALARE
//	Cosa si intende per valore? 
//		E' un'informazione elementare che sar� codificata 
//		attraverso un numero
public abstract class Card {
	// TUTTE le variabili di stato di una classe SONO PRIVATE
	private int seed;
	private int value;

	// L'accesso alle variabili di stato � garantito tramite
	// i GETTERS e i SETTERS
	public int getSeed() {
		return seed;
	}

	public int getValue() {
		return value;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

	public void setValue(int value) {
		this.value = value;
	}

	// Se vogliamo garantire la possibilit� di
	// confrontare le istanze di questa classe
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Card // controlla che obj sia una Card
				&& obj.hashCode() == this.hashCode();
	}

	@Override
	public int hashCode() {
		return seed * 1000 + value;
	}
}
