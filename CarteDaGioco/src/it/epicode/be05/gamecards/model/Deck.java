package it.epicode.be05.gamecards.model;

import java.util.ArrayList;
import java.util.List;

//Cosa � un mazzo di carte?
//Un insieme di carte da gioco.
//
//Mazzo di carte 1 <>-------- n carta da gioco
public class Deck {
	// poich� questa variabile potr� essere gestita
	// in una sottoclasse, non pu� essere privata
	// ma deve essere pubblica per le sottoclassi!
	protected List<Card> cards;
	
	public Deck() {
		// chi inizializza il contenitore delle carte?
		// quante carte ci stanno dentro?
		// Sospendo il discorso...
		
		cards = new ArrayList<>();
	}

	// Accessors Methods
	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	
	// Operations
	
	// azione che fornisce la prossima carta del mazzo
	public Card deal() {
		// devo implementare l'algoritmo
		// 1. prendo la carta che sta sopra al mazzo (se esiste)
		var card = cards // tutte le carte
				.stream() // apro uno stream su di esse
				.findFirst()// recupero la prima
				.orElse(null); // oppure d� null
		if (card != null) // se � stata recuperata...
			cards.remove(card); // la toglie dal mazzo...
		return card; // restituisce la carta se � stata recuperata
	}
	
	// aggiungo un metodo di supporto che mi dice se il 
	// mazzo di carte contiene ancora carte
	public boolean hasMoreCards() {
		return !cards.isEmpty();
	}
}
